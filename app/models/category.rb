class Category < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, use: [:slugged, :finders]
	
	include Tree
	include ScopeSortOrder

	has_many :articles, foreign_key: 'category_id'
	accepts_nested_attributes_for :articles
end
