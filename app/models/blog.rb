class Blog < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  is_impressionable
  
	belongs_to :user, foreign_key: 'user_id'
	belongs_to :category_blog, foreign_key: 'category_blog_id'

  has_many :comments, as: :commentable, dependent: :destroy
  accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true
  
	include ScopeSortOrder
	include ScopeVerified
  include VerifiedConfig

	has_attached_file :file, styles: { 
                      large:    '1024>', 
                      medium:   '256>', 
                      small:    '128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :file, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  scope :small_category, ->{eager_load(:category_blog)}

  private
    include FilterTitle
end
