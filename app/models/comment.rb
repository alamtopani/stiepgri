class Comment < ActiveRecord::Base
	belongs_to :user, foreign_key: 'user_id'
	belongs_to :commentable, polymorphic: true

	include ScopeSortOrder
	include ScopeVerified
	include VerifiedConfig
	
	default_scope {where(status: true)}
end
