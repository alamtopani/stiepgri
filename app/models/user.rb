class User < ActiveRecord::Base
	extend FriendlyId
  include UserConfig
  include UserAuthenticate
  include ScopeSortOrder
  include ScopeVerified
  include VerifiedConfig

  friendly_id :username, use: [:slugged, :finders]
end
