class Student < User
	extend FriendlyId
	friendly_id :username, use: [:slugged, :finders]
	
	default_scope {where(type: 'Student')}

	include FilterUser
end