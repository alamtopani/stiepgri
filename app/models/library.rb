class Library < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	
	include ScopeSortOrder

	private
		include FilterLibrary
end
