class Profile < ActiveRecord::Base
	after_initialize :after_initialized

	belongs_to :user, foreign_key: 'user_id'
	has_one :address, as: :addressable
	accepts_nested_attributes_for :address

	has_attached_file :avatar, styles: {
                      medium:   '256x256>', 
                      small:    '128x128>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :avatar, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  protected
		def after_initialized
			self.address = Address.new if self.address.blank?
		end
end
