class Page < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	is_impressionable

	include ScopeSortOrder
end
