class Event < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	is_impressionable
	
	belongs_to :user, foreign_key: 'user_id'
	include ScopeSortOrder
	include ScopeVerified
	include VerifiedConfig
	include FilterTitle

	has_many :comments, as: :commentable, dependent: :destroy
  accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true
end
