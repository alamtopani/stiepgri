class Facility < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	
	has_many :galleries, as: :galleriable
  accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true
  
	include ScopeSortOrder
end
