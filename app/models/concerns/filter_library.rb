module FilterLibrary
	extend ActiveSupport::Concern

	included do 
		
		scope :by_title, ->(title) do
			return if title.blank?
			query_options = [
				"LOWER(libraries.title) LIKE LOWER(:key)",
				"LOWER(libraries.author) LIKE LOWER(:key)",
				"LOWER(libraries.publisher) LIKE LOWER(:key)"
			].join(' OR ')
			where(query_options, key: "%#{title}%")
		end

		scope :filter_search, ->(params) do
			return all if params.blank?
			by_title(params[:title])
		end
	end
end