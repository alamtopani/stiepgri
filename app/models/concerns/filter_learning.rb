module FilterLearning
	extend ActiveSupport::Concern

	included do 
		scope :bonds, ->{eager_load(:user, :category_elearning)}
		
		scope :by_title, ->(title) do
			return if title.blank?
			query_options = [
				"LOWER(e_learnings.title) LIKE LOWER(:key)",
				"LOWER(category_elearnings.name) LIKE LOWER(:key)",
				"LOWER(users.username) LIKE LOWER(:key)"
			].join(' OR ')
			bonds.where(query_options, key: "%#{title}%")
		end

		scope :by_category, ->(category_elearning) do 
			return if category_elearning.blank?
			bonds.where("LOWER(category_elearnings.name) LIKE LOWER('#{category_elearning}')")
		end

		scope :filter_search, ->(params) do
			return all if params[:search].blank?
			by_title(params[:search][:title])
			.by_category(params[:search][:category_elearning])
		end
	end
end