module VerifiedConfig
	extend ActiveSupport::Concern

	def change_verified_status!
    if self.status
      self.status = false
      self.save
    elsif !self.status
      self.status = true
      self.save
    end
  end
end