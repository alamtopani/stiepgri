module FilterUser
	extend ActiveSupport::Concern

	included do 
		scope :bonds, ->{eager_load(:profile)}
		
		scope :by_name, ->(name) do
			return if name.blank?
			query_options = [
				"LOWER(profiles.full_name) LIKE LOWER(:key)",
				"LOWER(profiles.nim) LIKE LOWER(:key)"
			].join(' OR ')
			bonds.where(query_options, key: "%#{name}%")
		end

		scope :by_generation, ->(generation) do 
			return if generation.blank?
			bonds.where("profiles.generation =?", generation)
		end

		scope :by_study_program, ->(study_program) do 
			return if study_program.blank?
			bonds.where("profiles.study_program =?", study_program)
		end

		scope :filter_search, ->(params) do
			return all if params.blank?
			by_name(params[:name])
			.by_generation(params[:generation])
			.by_study_program(params[:study_program])
		end
	end
end