module FilterTitle
	extend ActiveSupport::Concern

	included do 
		scope :bonds, ->{eager_load(:user)}
		
		scope :by_title, ->(title) do
			return if title.blank?
			query_options = [
				"LOWER(#{self.name.downcase}s.title) LIKE LOWER(:key)",
				"LOWER(users.username) LIKE LOWER(:key)"
			].join(' OR ')
			bonds.where(query_options, key: "%#{title}%")
		end

		scope :by_category_type, ->(category_type) do 
			return if category_type.blank?
			where("#{self.name.downcase}s.category_type =?", category_type)
		end

		scope :by_category, ->(category_id) do 
			return if category_id.blank?
			where("#{self.name.downcase}s.category_id =?", category_id)
		end

		scope :by_category_blog, ->(category_blog) do 
			return if category_blog.blank?
			small_category.where("category_blogs.name =?", category_blog)
		end

		scope :filter_search, ->(params) do
			return all if params.blank?
			by_title(params[:title])
			.by_category_type(params[:category_type])
			.by_category(params[:category_id])
			.by_category_blog(params[:category_blog])
		end
	end
end