module UserConfig
	extend ActiveSupport::Concern

	included do
		devise  :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, #:confirmable,
         		:validatable, :lockable, :timeoutable, :omniauthable ,omniauth_providers: [:facebook], :authentication_keys => [:login]

    after_initialize :after_initialized
    before_save :downcase_username

		has_one :profile, foreign_key: 'user_id'
	  accepts_nested_attributes_for :profile

	  has_many :jobs, foreign_key: 'user_id'
	  accepts_nested_attributes_for :jobs

	  has_many :events, foreign_key: 'user_id'
	  accepts_nested_attributes_for :events

	  has_many :e_learnings, foreign_key: 'user_id'
	  accepts_nested_attributes_for :e_learnings

	  has_many :downloads, foreign_key: 'user_id'
	  accepts_nested_attributes_for :downloads

	  has_many :blogs, foreign_key: 'user_id'
	  accepts_nested_attributes_for :blogs

	  has_many :articles, foreign_key: 'user_id'
	  accepts_nested_attributes_for :articles

	  has_many :announcements, foreign_key: 'user_id'
	  accepts_nested_attributes_for :announcements

	  has_many :academic_infos, foreign_key: 'user_id'
	  accepts_nested_attributes_for :academic_infos

	  has_many :comments, foreign_key: 'user_id'
	  accepts_nested_attributes_for :comments
	  
	  scope :by_name, ->{order(name: :asc)}

	  validates_uniqueness_of :username, :email
	end

	def admin?
		self.type == "Admin"
	end

	def staff?
		self.type == "Staff"
	end

	def student?
		self.type == "Student"
	end

	def alumni?
		self.type == "Alumni"
	end

	def company?
		self.type == "UserCompany"
	end

	protected
  	def after_initialized
  		self.profile = Profile.new if profile.blank?
  	end

  	def downcase_username
			self.username = self.username.downcase if username_changed?
		end
end