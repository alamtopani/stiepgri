class CategoryElearning < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, use: [:slugged, :finders]
	
	include Tree
	include ScopeSortOrder

	has_many :e_learnings, foreign_key: 'category_learning_id'
	accepts_nested_attributes_for :e_learnings
end
