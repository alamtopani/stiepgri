class GuestBook < ActiveRecord::Base
	include ScopeSortOrder
	include ScopeVerified
	include VerifiedConfig

	def status?
		if self.status == true
    	return '<span class="label label-success">Already Checked</span>'.html_safe
    else
    	return '<span class="label label-warning">Not Yet Checked</span>'.html_safe
    end
  end
end
