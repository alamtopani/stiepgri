class AcademicInfo < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	is_impressionable
	
	belongs_to :user, foreign_key: 'user_id'
	include ScopeSortOrder
	include ScopeVerified
	include VerifiedConfig

	has_many :comments, as: :commentable, dependent: :destroy
  accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true

  scope :bonds, ->{eager_load(:user)}
	scope :by_title, ->(title) do
		return if title.blank?
		query_options = [
			"LOWER(academic_infos.title) LIKE LOWER(:key)",
			"LOWER(users.username) LIKE LOWER(:key)"
		].join(' OR ')
		bonds.where(query_options, key: "%#{title}%")
	end

	scope :filter_search, ->(params) do
		return all if params.blank?
		by_title(params[:title])
	end
end
