class Company < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, use: [:slugged, :finders]

	belongs_to :user_company, foreign_key: 'user_id'
end
