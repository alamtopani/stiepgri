class Download < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	
	belongs_to :user, foreign_key: 'user_id'

	include ScopeSortOrder
	include ScopeVerified
	include VerifiedConfig

	has_attached_file :file
  validates_attachment_content_type :file, :content_type => [ 'application/pdf','text/plain', "application/zip", "application/x-zip", "application/x-zip-compressed" ]

  private
		include FilterTitle
end
