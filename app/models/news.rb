class News < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  is_impressionable
  
	include ScopeSortOrder
	include ScopeVerified
  include VerifiedConfig

  has_many :comments, as: :commentable, dependent: :destroy
  accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true
  
	has_attached_file :file, styles: { 
                      large:    '700>', 
                      medium:   '256>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :file, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }
end
