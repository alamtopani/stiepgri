class Article < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	is_impressionable
	
	belongs_to :user, foreign_key: 'user_id'
	belongs_to :category, foreign_key: 'category_id'

	has_many :comments, as: :commentable, dependent: :destroy
  accepts_nested_attributes_for :comments, reject_if: :all_blank, allow_destroy: true
  
	include ScopeSortOrder
	include ScopeVerified
	include VerifiedConfig

	has_attached_file :file
  validates_attachment_content_type :file, :content_type => [ 'application/pdf','text/plain', "application/zip", "application/x-zip", "application/x-zip-compressed" ]

  private
		include FilterTitle
end
