class UserCompany < User
	extend FriendlyId
	friendly_id :username, use: [:slugged, :finders]
	
	after_initialize :build_form_company
	default_scope {where(type: 'UserCompany')}

	has_one :company, foreign_key: 'user_id'
	accepts_nested_attributes_for :company

	protected
		def build_form_company
  		self.company = Company.new if company.blank?
  	end
end