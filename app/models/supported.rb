class Supported < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]
  
	include ScopeSortOrder

	has_attached_file :file, styles: { 
                      large:    '1024>', 
                      medium:   '256>', 
                      small:    '128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :file, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }
end
