class CategoryBlog < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, use: [:slugged, :finders]

	include Tree
	include ScopeSortOrder

	has_many :blogs, foreign_key: 'category_blog_id'
	accepts_nested_attributes_for :blogs
end
