class Alumni < User
	extend FriendlyId
	friendly_id :username, use: [:slugged, :finders]
	
	default_scope {where(type: 'Alumni')}

	include FilterUser
end