class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    elsif user.staff? && user.status == true
      can :manage, User, id: user.id
      can :manage, Event, id: user.id
      can :manage, AcademicInfo, user_id: user.id
      can :manage, Announcement, user_id: user.id
      can :manage, Article, user_id: user.id
      can :manage, Category
      can :manage, Blog, user_id: user.id
      can :manage, CategoryBlog
      can :manage, Download, user_id: user.id
      can :manage, ELearning, user_id: user.id
      can :manage, CategoryElearning
    elsif user.company? && user.status == true
      can :manage, User, id: user.id
      can :manage, Job, user_id: user.id
    elsif user.alumni? && user.status == true
      can :manage, User, id: user.id
      can :manage, Event, id: user.id
      can :manage, Article, user_id: user.id
      can :manage, Blog, user_id: user.id
      can :manage, Job, user_id: user.id
    elsif user.student? && user.status == true
      can :manage, User, id: user.id
      can :manage, Event, id: user.id
      can :manage, Article, user_id: user.id
      can :manage, Blog, user_id: user.id
    end
  end
end
