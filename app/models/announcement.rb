class Announcement < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]
	
	belongs_to :user, foreign_key: 'user_id'
	include ScopeSortOrder
	include ScopeVerified
	include VerifiedConfig
	include FilterTitle
end
