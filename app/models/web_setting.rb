class WebSetting < ActiveRecord::Base
	has_many :galleries, as: :galleriable
  accepts_nested_attributes_for :galleries, reject_if: :all_blank, allow_destroy: true

	include ScopeSortOrder

	has_attached_file :favicon, styles: { 
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :favicon, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }

  has_attached_file :logo, styles: { 
                      large:    '512x512>', 
                      medium:   '256x256>', 
                      small:    '128x128>',
                      thumb:    '64x64>'
                    }, 
                    default_url: 'no-image.png'
                                    
  validates_attachment :logo, content_type: { 
    content_type: ["image/jpg", "image/jpeg", "image/png"] 
  }
end
