module ScopeVerified
	extend ActiveSupport::Concern
	included do 
		scope :verified, ->{where(status: true)}
	  scope :unverified, ->{where(status: false)}
	  scope :not_checked, ->{where(status: nil)}
	end

	def status?
		if self.status == true
    	return '<span class="label label-success">Already Published</span>'.html_safe
    else
    	return '<span class="label label-warning">Not Yet Published</span>'.html_safe
    end
  end

end