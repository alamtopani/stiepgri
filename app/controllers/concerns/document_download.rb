module DocumentDownload
	extend ActiveSupport::Concern

	included do 
		def document_download
		  resource = collection.find(params[:id])
	    url = resource.file.url
	    file_path = url.split("?").first
	    if !file_path.nil?
				send_file "#{Rails.root}/public#{file_path}", :x_sendfile => true 
			else 
	       redirect_to :back
			end
		end
	end
end