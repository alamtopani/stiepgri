module DeviseFilter
  extend ActiveSupport::Concern

	included do
		after_filter :store_location
		before_filter :configure_permitted_parameters, if: :devise_controller?
	end

	def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u|
      u.permit(:username, :email, :password, :password_confirmation, :type)
    }
  end

  def store_location
    return if !request.get? || request.xhr?
    return if request.fullpath.match("/users")
  	if request.format == "text/html" || request.content_type == "text/html"
      session[:previous_url] = request.fullpath
      session[:last_request_time] = Time.now.utc.to_i
    end
  end

	def after_sign_in_path_for(resource)
    if current_user.admin? || current_user.staff?
      return backend_home_path 
    else
      session[:previous_url] || root_path
    end
  end

  # note : after_sign_up_path override in registration_controller

  def authenticate_this_user!
    unless current_user.present?
      redirect_to root_path, alert: "Can't Access this page"
    end 
  end

  def after_sign_out_path_for(resource_or_scope)
    root_url
  end

end