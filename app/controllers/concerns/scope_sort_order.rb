module ScopeSortOrder
	extend ActiveSupport::Concern
	included do 
		scope :latest, ->{order(created_at: :desc)}
	  scope :oldest, ->{order(created_at: :asc)}
	  scope :updatest, ->{order(updated_at: :asc)}
	end
end