module ActionMultiple
	extend ActiveSupport::Concern

	def action_multiple
		if params[:commit] == 'Deleted'
			destroy_multiple
		elsif params[:commit] == 'Change Verify Selected'
			verified_multiple
		end	
	end

	def verified_multiple
		if params[:ids].present?
			@collection = collection.where("id IN (?)", params[:ids])
			@collection.each do |resource|
				resource.change_verified_status!
			end
			redirect_to :back, notice: "You verified multiple successfully change!"
		else
			redirect_to :back, alert: 'Please Choice Selected!'
		end
	end

	def destroy_multiple
		if params[:ids].present?
			@collection = collection.destroy(params[:ids])
			if @collection
				redirect_to :back, notice: 'Successfully Destroyed!'
			else
				redirect_to :back, alert: 'Not Successfully Destroyed!'
			end
		else
			redirect_to :back, alert: 'Please Choice Selected!'
		end
	end
end