module Breadcrumb 
	extend ActiveSupport::Concern

	included do 
		add_breadcrumb "Home", :root_path
		add_breadcrumb "#{resource_class}", :collection_path
	end
end