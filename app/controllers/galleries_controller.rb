class GalleriesController < ResourcesController
	defaults resource_class: Gallery, collection_name: 'galleries', instance_name: 'gallery'
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end