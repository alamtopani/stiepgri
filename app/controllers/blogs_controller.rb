class BlogsController < ResourcesController
	defaults resource_class: Blog, collection_name: 'blogs', instance_name: 'blog'
	include Breadcrumb

	def index
		if params[:blog_category]
			@collection = collection.small_category.where("category_blogs.name =?", params[:blog_category])
		else
			@collection = collection
		end

		@collection = @collection.verified.latest.page(page).per(4)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
		impressionist(resource)
		@comment = Comment.new
		@comments = resource.comments.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end