class FacilitiesController < ResourcesController
	defaults resource_class: Facility, collection_name: 'facilities', instance_name: 'facility'
	include Breadcrumb

	def index
		@collection = collection.latest.page(page).per(9)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end