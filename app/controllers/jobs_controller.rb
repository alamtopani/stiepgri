class JobsController < ResourcesController
	defaults resource_class: Job, collection_name: 'jobs', instance_name: 'job'
	include Breadcrumb

	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end