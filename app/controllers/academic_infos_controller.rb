class AcademicInfosController < ResourcesController
	defaults resource_class: AcademicInfo, collection_name: 'academic_infos', instance_name: 'academic_info'
	include Breadcrumb

	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
		impressionist(resource)
		@comment = Comment.new
		@comments = resource.comments.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end