class RegistrationsController < Devise::RegistrationsController
	before_filter :configure_permitted_parameters

	protected

	  def configure_permitted_parameters
	    devise_parameter_sanitizer.for(:sign_up) { |u|
	      u.permit(
	      					:username, 
	      					:email, 
	      					:password, 
	      					:password_confirmation, 
	      					:type,
	      					:status,
	      					profile_attributes: [
						        :id,
						        :user_id,
						        :full_name,
						        :birthday,
						        :gender,
						        :phone,
						        :avatar,
						        :study_program,
						        :generation,
						        :nim
						      ]
	      				)
	    }
	    devise_parameter_sanitizer.for(:sign_in) { |u| 
	    	u.permit(:login, :username, :email, :password, :remember_me) 
	    }
	  end
end