class LibrariesController < ResourcesController
	defaults resource_class: Library, collection_name: 'libraries', instance_name: 'library'
	include Breadcrumb

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end