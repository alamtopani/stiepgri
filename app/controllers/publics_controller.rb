class PublicsController < ApplicationController
	add_breadcrumb "Home", :root_path

	def home
	end

	def rekruitment
		add_breadcrumb "Job Placement Center"
	end

	def contact_us
		add_breadcrumb "Kontak Kami"
	end
end