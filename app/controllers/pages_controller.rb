class PagesController < ResourcesController
	defaults resource_class: Page, collection_name: 'pages', instance_name: 'page'
	include Breadcrumb

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
		impressionist(resource)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end