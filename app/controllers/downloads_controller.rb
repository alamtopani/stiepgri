class DownloadsController < ResourcesController
	defaults resource_class: Download, collection_name: 'downloads', instance_name: 'download'
	include DocumentDownload
	include Breadcrumb

	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end