class AlumnisController < ResourcesController
	defaults resource_class: Alumni, collection_name: 'alumnis', instance_name: 'alumni'
	include Breadcrumb
	
	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.try(:profile).try(:nim)}", resource_path
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end