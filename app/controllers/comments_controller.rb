class CommentsController < ResourcesController
	defaults resource_class: Comment, collection_name: 'comments', instance_name: 'comment'
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def create
		build_resource
		resource.user_id = current_user.id
		create! do |format|
			if resource.errors.empty?
				format.html { redirect_to :back }
			else
				flash[:errors] = resource.errors.full_messages
        format.html { redirect_to :back }
			end
		end
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end