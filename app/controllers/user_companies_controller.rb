class UserCompaniesController < ResourcesController
	defaults resource_class: UserCompany, collection_name: 'user_companies', instance_name: 'user_company'
	include Breadcrumb

	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.try(:company).try(:name)}", resource_path
	end

	def new
		add_breadcrumb "Register", new_resource_path
		@user_company = UserCompany.new
	end

	def create
		build_resource
		resource.status = false
		create! do |format|
			if resource.errors.empty?
				flash[:alert] = 'Perusahaan anda berhasil terdaftar, akun anda akan aktif jika telah di verifikasi admin! silahkan hubungi admin kami!'
				format.html { redirect_to :back }
			else
				flash[:errors] = resource.errors.full_messages
				format.html { redirect_to :back }
			end
		end
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end