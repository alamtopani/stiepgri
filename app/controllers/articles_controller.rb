class ArticlesController < ResourcesController
	defaults resource_class: Article, collection_name: 'articles', instance_name: 'article'
	include DocumentDownload
	include Breadcrumb

	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
		impressionist(resource)
		@comment = Comment.new
		@comments = resource.comments.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end