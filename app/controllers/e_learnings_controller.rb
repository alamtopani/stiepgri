class ELearningsController < ResourcesController
	defaults resource_class: ELearning, collection_name: 'e_learnings', instance_name: 'e_learning'
	include DocumentDownload
	include Breadcrumb

	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end