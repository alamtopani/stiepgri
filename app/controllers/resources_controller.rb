class ResourcesController < ApplicationController
  inherit_resources
	include Backend::Parameters

	def create
		create! do |success, failure|
			success.html { redirect_to collection_path }
		end
	end

	def update
		update! do |success, failure|
			success.html { redirect_to collection_path }
		end
	end	
end