class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
  
  protect_from_forgery with: :exception

  include DeviseFilter

  before_filter :prepare_settings

  protected
  	def per_page
      params[:per_page] ||= 18
    end

    def page
      params[:page] ||= 1
    end

    def prepare_settings
      @setting = WebSetting.first
      @latest_academic = AcademicInfo.latest.verified.limit(5)
      @latest_news = News.latest.verified.limit(4)
      @events = Event.latest.verified.limit(4)
      @announcements = Announcement.latest.verified
      @supporteds = Supported.latest
      @galleries = Gallery.latest.limit(8)

      @about_us = Page.where(category: 'About Us').oldest
      @study_program = Page.where(category: 'Study Program Information').oldest
      @facilities = Facility.oldest
      @category_blogs = CategoryBlog.latest
    end
end
