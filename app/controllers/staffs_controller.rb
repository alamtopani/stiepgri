class StaffsController < ResourcesController
	defaults resource_class: Staff, collection_name: 'staffs', instance_name: 'staff'
	include Breadcrumb
	
	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.try(:profile).try(:full_name)}", resource_path
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end