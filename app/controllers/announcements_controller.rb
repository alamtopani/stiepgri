class AnnouncementsController < ResourcesController
	defaults resource_class: Announcement, collection_name: 'announcements', instance_name: 'announcement'
	include Breadcrumb

	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
	end
	
	private
		def collection
			@collection ||= end_of_association_chain
		end
end