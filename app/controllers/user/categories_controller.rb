class User::CategoriesController < User::HomeController
	load_and_authorize_resource
	defaults resource_class: Category, collection_name: 'categories', instance_name: 'category'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end