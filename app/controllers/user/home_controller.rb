class User::HomeController < User::ResourcesController
	layout 'user'
	before_filter :authenticate_admin!
	protect_from_forgery with: :exception
	
	def beranda
	end

	def authenticate_admin!
  	unless current_user.present? && (current_user.student?) || current_user.present? && (current_user.alumni?) || current_user.present? && (current_user.company?)
      redirect_to root_path 
    end 
  end

end