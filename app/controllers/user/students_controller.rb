class User::StudentsController < User::UsersController
	defaults resource_class: Student, collection_name: 'students', instance_name: 'student'
	include ActionMultiple

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end