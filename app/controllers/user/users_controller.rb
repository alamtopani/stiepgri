class User::UsersController < User::HomeController
	load_and_authorize_resource
	defaults resource_class: User, collection_name: 'users', instance_name: 'user'
	include UserFilter
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def update
		update! do |format|
			if resource.errors.empty?
				format.html{ redirect_to :back }
			else
				flash[:errors] = resource.errors.full_messages
				format.html{ redirect_to :back }
			end
		end
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end