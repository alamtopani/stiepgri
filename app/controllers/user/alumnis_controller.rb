class User::AlumnisController < User::UsersController
	defaults resource_class: Alumni, collection_name: 'alumnis', instance_name: 'alumni'
	include ActionMultiple

	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end