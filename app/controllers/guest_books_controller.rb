class GuestBooksController < ResourcesController
	defaults resource_class: GuestBook, collection_name: 'guest_books', instance_name: 'guest_book'
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def create
		build_resource
		if verify_recaptcha(model: resource, message: "Please enter the correct captcha!") && resource.save
			redirect_to :back, notice: 'Your message Successfully create'
		else
      redirect_to :back, error: 'Not Successfully create'
		end
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end