class NewsController < ResourcesController
	defaults resource_class: News, collection_name: 'news', instance_name: 'news'
	include Breadcrumb

	def index
		@collection = collection.verified.latest.page(page).per(per_page)
	end

	def show
		add_breadcrumb "#{resource.title}", resource_path
		impressionist(resource)
		@comment = Comment.new
		@comments = resource.comments.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end