class Backend::GuestBooksController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: GuestBook, collection_name: 'guest_books', instance_name: 'guest_book'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	def show
		if resource
			@guest_book = collection.find(resource)
			@guest_book.update(status: true)
			@guest_book.save
		end
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end