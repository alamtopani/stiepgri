class Backend::CategoryBlogsController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: CategoryBlog, collection_name: 'category_blogs', instance_name: 'category_blog'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end