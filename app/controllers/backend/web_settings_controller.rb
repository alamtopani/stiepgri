class Backend::WebSettingsController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: WebSetting, collection_name: 'web_settings', instance_name: 'web_setting'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end