class Backend::CommentsController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: Comment, collection_name: 'comments', instance_name: 'comment'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end