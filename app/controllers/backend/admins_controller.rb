class Backend::AdminsController < Backend::UsersController
	defaults resource_class: Admin, collection_name: 'admins', instance_name: 'admin'
	include ActionMultiple

	def index
		@collection = collection.where("id != ?", current_user.id).latest.page(page).per(per_page)
	end
end