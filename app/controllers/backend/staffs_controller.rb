class Backend::StaffsController < Backend::UsersController
	defaults resource_class: Staff, collection_name: 'staffs', instance_name: 'staff'
	include ActionMultiple

	def index
		@collection = collection.where("id != ?", current_user.id).latest.page(page).per(per_page)
	end
end