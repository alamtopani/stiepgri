class Backend::PagesController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: Page, collection_name: 'pages', instance_name: 'page'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end