class Backend::UserCompaniesController < Backend::UsersController
	defaults resource_class: UserCompany, collection_name: 'user_companies', instance_name: 'user_company'
	include ActionMultiple

	def index
		@collection = collection.where("id != ?", current_user.id).latest.page(page).per(per_page)
	end
end