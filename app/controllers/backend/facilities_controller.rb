class Backend::FacilitiesController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: Facility, collection_name: 'facilities', instance_name: 'facility'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end