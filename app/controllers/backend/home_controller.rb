class Backend::HomeController < Backend::ResourcesController
	layout 'backend'
	before_filter :authenticate_admin!
	protect_from_forgery with: :exception
	
	def beranda
	end

	def authenticate_admin!
  	unless current_user.present? && (current_user.admin?) || current_user.present? && (current_user.staff?)
      redirect_to root_path 
    end 
  end

end