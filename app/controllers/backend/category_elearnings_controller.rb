class Backend::CategoryElearningsController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: CategoryElearning, collection_name: 'category_elearnings', instance_name: 'category_elearning'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end