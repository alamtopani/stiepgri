class Backend::SupportedsController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: Supported, collection_name: 'supporteds', instance_name: 'supported'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain
		end
end