class Backend::AcademicInfosController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: AcademicInfo, collection_name: 'academic_infos', instance_name: 'academic_info'
	include ActionMultiple
	
	def create
		build_resource
		resource.user_id = current_user.id
		create! do |format|
			if resource.errors.empty?
				format.html{ redirect_to collection_path }
			else
				flash[:errors] = resource.errors.full_messages
				format.html{ redirect_to :back }
			end
		end
	end

	def index
		@collection = collection.latest.page(page).per(per_page)

		unless current_user.admin?
			@collection = @collection.where("academic_infos.user_id =?", current_user.id)
		end
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end