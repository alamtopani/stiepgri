class Backend::LibrariesController < Backend::HomeController
	load_and_authorize_resource
	defaults resource_class: Library, collection_name: 'libraries', instance_name: 'library'
	include ActionMultiple
	
	def index
		@collection = collection.latest.page(page).per(per_page)
	end

	private
		def collection
			@collection ||= end_of_association_chain.filter_search(params)
		end
end