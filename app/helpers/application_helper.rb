module ApplicationHelper
  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end
  
  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} alert-dismissible", role: 'alert') do
        concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
          concat content_tag(:span, '&times;'.html_safe, 'aria-hidden' => true)
          concat content_tag(:span, 'Close', class: 'sr-only')
        end)
        if flash[:errors].present?
          message.each do |word|
            concat "<div class='alert alert-danger alert-dismissible margin-bottom5'>#{word}</div>".html_safe
          end
        else
          concat message
        end
      end)
    end
    nil
  end
	def active_sidebar?(controller, *actions)
    if actions.include?(params[:action].to_sym) || actions.include?(:all)
      return 'active'
    end if controller_name == controller.to_s
  end

  def category_type_blog_articles
    @category_type_blog_articles ||= ['Article','Journal']
  end
  def sex
    @sex ||= ['Laki-Laki','Perempuan']
  end
  def landing_page_category
  	@landing_page_category ||= ['About Us','Study Program Information']
  end
  def user_type
    @user_type ||= [['Mahasiswa','Student'],['Alumni','Alumni']]
  end
  def user_program
    @user_program ||= ['Akuntansi','Manajemen','Ekonomi']
  end
  def user_generation
    @user_generation ||= (2000..Time.new.year).to_a.sort.reverse.map{|y| [y, y]}
  end

  def latest_comments
    @latest_comments ||= GuestBook.latest
  end
  def latest_companies
    @latest_companies ||= UserCompany.latest
  end
  def latest_staff
    @latest_staff ||= Staff.latest
  end
  def latest_alumnus
    @latest_alumnus ||= Alumni.latest
  end
  def latest_students
    @latest_students ||= Student.latest
  end

end
