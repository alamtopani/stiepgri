require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'devise'
require 'paperclip'
# require 'ckeditor_rails'
require 'ckeditor'
require 'country_select'

Bundler.require(*Rails.groups)

module Stiepgri
  class Application < Rails::Application
    config.i18n.available_locales = [:id, :en]
    config.i18n.default_locale = :en
    config.assets.enabled = true
    config.assets.paths << Rails.root.join("app", "assets", "fonts")
    config.autoload_paths += %W(#{config.root}/lib)
    config.assets.paths << Rails.root.join("vendor", "assets", "fonts")
    config.assets.paths << Rails.root.join("vendor", "assets", "images")
    config.assets.paths << Rails.root.join("vendor", "assets", "stylesheets")
    config.assets.paths << Rails.root.join("vendor", "assets", "javascripts")
    config.assets.initialize_on_precompile = true
    config.action_dispatch.ignore_accept_header = true
    config.assets.precompile += %w(.svg .eot .woff .ttf backend.js backend.css, devise.css)  
    config.quiet_assets = true
    config.action_dispatch.default_headers = {
        'X-Frame-Options' => 'ALLOWALL',
        'Vary' => 'Accept-Encoding'
    }
    config.middleware.use HtmlCompressor::Rack, {preserve_line_breaks: false,javascript_compressor: :ugglifier, css_compressor: :sass, remove_intertag_spaces: true, remove_quotes: false}
    config.assets.precompile += Ckeditor.assets
    config.assets.precompile += %w( ckeditor/* )
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
  end
end
