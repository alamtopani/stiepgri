# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'Stiepgri'
set :repo_url, 'git@gitlab.com:alamtopani/stiepgri.git'
set :scm, :git
set :deploy_to, '/home/stiepgri/www'

set :format, :pretty
set :log_level, :info

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp vendor/bundle public/system}

set :rbenv_path, '/home/stiepgri/.rbenv'

set :default_env, { 
   path: "/home/stiepgri/.rbenv/shims:/home/stiepgri/.rbenv/bin:$PATH"
 }
 
set :keep_releases, 3

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'unicorn:restart'
    end
  end

  after :finishing, 'deploy:cleanup'
end

after 'deploy:publishing', 'deploy:restart'