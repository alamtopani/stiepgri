# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
# Don't declare `role :all`, it's a meta role
role :app, %w{stiepgri@23.92.53.195}
role :web, %w{stiepgri@23.92.53.195}
role :db,  %w{stiepgri@23.92.53.195}

set :stage, :production

# Replace 127.0.0.1 with your server's IP address!
server '23.92.53.195', user: 'stiepgri', roles: %w{web app}

set :rbenv_type,     :system
set :rbenv_ruby,     '2.1.1'
set :rbenv_prefix,   "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles,    :all

set :deploy_to,       "/home/stiepgri/www"
set :rails_env,       "production"
set :branch,          "master"

set :unicorn_config_path, "/home/stiepgri/www/current/config/unicorn.rb"
