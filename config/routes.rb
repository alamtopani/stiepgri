Stiepgri::Application.routes.draw do
  root 'publics#home'
  get '/rekruitment',  to: 'publics#rekruitment',  as: 'rekruitment'
  get '/kontak-kami',  to: 'publics#contact_us',  as: 'contact_us'

  devise_for :users, 
    controllers: { 
      omniauth_callbacks: 'omniauth_callbacks',
      registrations: 'registrations',
      sessions: 'sessions',
      confirmations: "confirmations"
    }, 
    path_names: { 
      sign_in:  'login', 
      sign_out: 'logout', 
      sign_up:  'register'
    }

  concern :startup_backend, Startup::BackendRoutes.new
  namespace :backend do
    concerns :startup_backend
  end

  concern :startup_user, Startup::UserRoutes.new
  namespace :user do
    concerns :startup_user
  end

  resources :staffs
  resources :alumnis
  resources :students
  resources :user_companies
  resources :pages
  resources :news
  resources :libraries
  resources :jobs
  resources :guest_books
  resources :facilities
  resources :events
  resources :e_learnings do 
    member do 
      get 'document_download'
    end
  end
  resources :category_elearnings
  resources :downloads do 
    member do 
      get 'document_download'
    end
  end
  resources :companies
  resources :category_blogs
  resources :blogs
  resources :articles do 
    member do 
      get 'document_download'
    end
  end
  resources :categories
  resources :announcements
  resources :academic_infos
  resources :comments
  
  # namespace :user do
  #   resources :users
  #   resources :jobs
  #   resources :events
  #   resources :e_learnings
  #   resources :downloads
  #   resources :blogs
  #   resources :articles
  #   resources :announcements
  #   resources :academic_infos
  # end

end
