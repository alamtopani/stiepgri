module Permitable
	USER = [
    :id,
    :type,
    :username, 
    :email, 
    :password, 
    :password_confirmation, 
    :provider, 
    :uid,
    :slug,
    :status
  ]

  ADDRESS = {
      address_attributes: [
        :id,
        :address,
        :city,
        :province,
        :country,
        :postcode,
        :addressable_type,
        :addressable_id,
        :addressable,
        :facebook_url,
        :twitter_url
      ]
  }

  PROFILE = {
      profile_attributes: [
        :id,
        :user_id,
        :full_name,
        :birthday,
        :gender,
        :phone,
        :avatar,
        :banner_user,
        :about_me,
        :study_program,
        :generation,
        :nim
      ].push(ADDRESS.dup)
  }

  ACADEMIC_INFO = [
    :id,
    :title,
    :description,
    :status,
    :user_id
  ]

  ANNOUNCEMENT = [
    :id,
    :title,
    :description,
    :status,
    :user_id
  ]

  ARTICLE = [
    :id,
    :title,
    :description,
    :user_id,
    :status,
    :category_id,
    :file,
    :category_type
  ]

  CATEGORY = [
    :id,
    :name,
    :ancestry,
    :parent_id
  ]

  BLOG = [
    :id,
    :title,
    :description,
    :category_blog_id,
    :user_id,
    :status,
    :file
  ]

  CATEGORY_BLOG = [
    :id,
    :name,
    :ancestry,
    :parent_id
  ]

  COMPANY = {
    company_attributes: [
      :id,
      :name,
      :website,
      :user_id,
      :description,
      :address,
      :phone
    ]
  }

  DOWNLOAD = [
    :id,
    :title,
    :file,
    :status,
    :user_id
  ]

  E_LEARNING = [
    :id,
    :title,
    :category_learning_id,
    :file,
    :status,
    :user_id
  ]

  CATEGORY_ELEARNING = [
    :id,
    :name,
    :ancestry,
    :parent_id
  ]

  EVENT = [
    :id,
    :title,
    :description,
    :status,
    :event_at,
    :place,
    :user_id
  ]

  FACILITY = [
    :id,
    :title,
    :description
  ]

  GALLERY = {
      galleries_attributes: [
        :id,
        :title,
        :description,
        :file,
        :galleriable_type,
        :galleriable_id,
        :galleriable,
        :_destroy
      ]
  }

  GUEST_BOOK = [
    :id,
    :name,
    :email,
    :message,
    :status
  ]

  JOB = [
    :id,
    :title,
    :position,
    :description,
    :start_at,
    :end_at,
    :status,
    :user_id,
    :image,
    :website
  ]

  LIBRARY = [
    :id,
    :title,
    :author,
    :publisher
  ]

  NEWS = [
    :id,
    :title,
    :description,
    :status,
    :file
  ]

  PAGE = [
    :id,
    :title,
    :description,
    :category
  ]

  SUPPORTED = [
    :id,
    :file,
    :link_url
  ]

  WEB_SETTING = [
      :id,
      :title,
      :description,
      :keywords,
      :header_tags,
      :footer_tags,
      :contact,
      :email,
      :favicon,
      :logo,
      :facebook,
      :twitter
  ]

  COMMENT = [
      :id,
      :user_id,
      :commentable_type,
      :commentable_id,
      :commentable,
      :comment,
      :status
  ]

  def self.controller(name)
    self.send name.gsub(/\W/,'_').singularize.downcase
  end

  def self.backend_user
    USER.dup.push(PROFILE.dup)
  end

  def self.backend_admin
    backend_user
  end

  def self.backend_student
    backend_user
  end

  def self.backend_alumni
    backend_user
  end

  def self.backend_staff
    backend_user
  end

  def self.backend_academic_info
    ACADEMIC_INFO.dup
  end

  def self.backend_announcement
    ANNOUNCEMENT.dup
  end

  def self.backend_article
    ARTICLE.dup
  end

  def self.backend_category
    CATEGORY.dup
  end

  def self.backend_blog
    BLOG.dup
  end

  def self.backend_category_blog
    CATEGORY_BLOG.dup
  end

  def self.backend_user_company
    backend_user.push(COMPANY.dup)
  end

  def self.backend_download
    DOWNLOAD.dup
  end

  def self.backend_e_learning
    E_LEARNING.dup
  end

  def self.backend_category_elearning
    CATEGORY_ELEARNING.dup
  end

  def self.backend_event
    EVENT.dup
  end

  def self.backend_facility
    FACILITY.dup.push(GALLERY.dup)
  end

  def self.backend_guest_book
    GUEST_BOOK.dup
  end

  def self.backend_comment
    COMMENT.dup
  end

  def self.backend_job
    JOB.dup
  end

  def self.backend_library
    LIBRARY.dup
  end

  def self.backend_news
    NEWS.dup
  end

  def self.backend_page
    PAGE.dup
  end

  def self.backend_supported
    SUPPORTED.dup
  end

  def self.backend_web_setting
    WEB_SETTING.dup.push(GALLERY.dup)
  end




    def self.user_user
    USER.dup.push(PROFILE.dup)
  end

  def self.user_student
    user_user
  end

  def self.user_alumni
    user_user
  end

  def self.user_article
    ARTICLE.dup
  end

  def self.user_category
    CATEGORY.dup
  end

  def self.user_blog
    BLOG.dup
  end

  def self.user_category_blog
    CATEGORY_BLOG.dup
  end

  def self.user_user_company
    user_user.push(COMPANY.dup)
  end

  def self.user_event
    EVENT.dup
  end

  def self.user_job
    JOB.dup
  end

  def self.user_library
    LIBRARY.dup
  end



  def self.guest_book
    GUEST_BOOK.dup
  end

  def self.comment
    backend_comment
  end

  def self.user_company
    backend_user.push(COMPANY.dup)
  end

end