module Startup
	class UserRoutes
		def call mapper, options={}
	    mapper.get '/home',                   to: 'home#beranda',                     as: 'home'
	    mapper.resources :students do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :alumnis do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :user_companies do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :events do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :jobs do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :blogs do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :articles do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	      mapper.member do 
	        mapper.get 'document_download'
	      end
	    end
	  end
	end
end