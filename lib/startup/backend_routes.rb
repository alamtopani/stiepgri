module Startup
	class BackendRoutes
		def call mapper, options={}
	    mapper.get '/home',                   to: 'home#beranda',                     as: 'home'
	    mapper.resources :users
	    mapper.resources :admins do
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :staffs do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :students do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :alumnis do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :user_companies do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :supporteds do 
	      mapper.collection do
	        mapper.delete 'destroy_multiple'
	      end
	    end
	  	mapper.resources :pages do 
	      mapper.collection do
	        mapper.delete 'destroy_multiple'
	      end
	    end
	  	mapper.resources :news do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :libraries do 
	      mapper.collection do
	        mapper.delete 'destroy_multiple'
	      end
	    end
	  	mapper.resources :jobs do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :guest_books do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :facilities do 
	      mapper.collection do
	        mapper.delete 'destroy_multiple'
	      end
	    end
	  	mapper.resources :events do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :e_learnings do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	      mapper.member do 
	        mapper.get 'document_download'
	      end
	    end
	    mapper.resources :category_elearnings do 
	      mapper.collection do
	        mapper.delete 'destroy_multiple'
	      end
	    end
	  	mapper.resources :downloads do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	      mapper.member do 
	        mapper.get 'document_download'
	      end
	    end
	  	mapper.resources :companies do 
	      mapper.collection do
	        mapper.delete 'destroy_multiple'
	      end
	    end
	  	mapper.resources :category_blogs do 
	      mapper.collection do
	        mapper.delete 'destroy_multiple'
	      end
	    end
	  	mapper.resources :blogs do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :articles do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	      mapper.member do 
	        mapper.get 'document_download'
	      end
	    end
	    mapper.resources :categories do 
	      mapper.collection do
	        mapper.delete 'destroy_multiple'
	      end
	    end
	  	mapper.resources :announcements do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	  	mapper.resources :academic_infos do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :comments do 
	      mapper.collection do
	        mapper.post 'action_multiple'
	      end
	    end
	    mapper.resources :web_settings
	  end
	end
end