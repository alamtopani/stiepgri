class CreateDownloads < ActiveRecord::Migration
  def change
    create_table :downloads do |t|
      t.string :title
      t.attachment :file
      t.boolean :status
      t.integer :user_id

      t.timestamps
    end

    add_index :downloads, :user_id
  end
end
