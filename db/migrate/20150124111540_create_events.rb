class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :description
      t.boolean :status
      t.date :event_at
      t.string :place
      t.integer :user_id

      t.timestamps
    end

    add_index :events, :user_id
  end
end
