class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.string :title
      t.text :description
      t.attachment :file
      t.string :galleriable_type
      t.integer :galleriable_id

      t.timestamps
    end
    add_index :galleries, :galleriable_type
    add_index :galleries, :galleriable_id
  end
end
