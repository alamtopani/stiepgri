class AddSlugInAllModel < ActiveRecord::Migration
  def change
  	add_column :pages, :slug, :string
  	add_column :blogs, :slug, :string
  	add_column :facilities, :slug, :string
  	add_column :articles, :slug, :string
  	add_column :academic_infos, :slug, :string
  	add_column :events, :slug, :string
  	add_column :news, :slug, :string
  	add_column :announcements, :slug, :string
  	add_column :supporteds, :slug, :string
  	add_column :downloads, :slug, :string
  	add_column :libraries, :slug, :string
  	add_column :companies, :slug, :string
  	add_column :jobs, :slug, :string
  	add_column :e_learnings, :slug, :string
  	add_column :categories, :slug, :string
  	add_column :category_elearnings, :slug, :string
  	add_column :category_blogs, :slug, :string
  end
end
