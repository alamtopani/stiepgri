class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :description
      t.integer :category_blog_id
      t.integer :user_id
      t.boolean :status
      t.attachment :file

      t.timestamps
    end

    add_index :blogs, :user_id
    add_index :blogs, :category_blog_id
  end
end