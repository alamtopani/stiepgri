class CreateELearnings < ActiveRecord::Migration
  def change
    create_table :e_learnings do |t|
      t.string :title
      t.attachment :file
      t.integer :category_learning_id
      t.boolean :status
      t.integer :user_id

      t.timestamps
    end

    add_index :e_learnings, :user_id
    add_index :e_learnings, :category_learning_id
  end
end
