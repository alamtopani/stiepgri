class AddStudyProgramAndGenerationInProfile < ActiveRecord::Migration
  def change
  	add_column :profiles, :study_program, :string
  	add_column :profiles, :generation, :string
  	add_column :profiles, :nim, :string
  end
end
