class CreateCategoryElearnings < ActiveRecord::Migration
  def change
    create_table :category_elearnings do |t|
      t.string :name
      t.string :ancestry

      t.timestamps
    end
  end
end
