class CreateLibraries < ActiveRecord::Migration
  def change
    create_table :libraries do |t|
      t.string :title
      t.string :author
      t.string :publisher

      t.timestamps
    end
  end
end
