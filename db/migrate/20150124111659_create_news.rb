class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.text :description
      t.boolean :status
      t.attachment :file

      t.timestamps
    end
  end
end
