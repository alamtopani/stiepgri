class CreateAcademicInfos < ActiveRecord::Migration
  def change
    create_table :academic_infos do |t|
      t.string :title
      t.text :description
      t.boolean :status
      t.integer :user_id

      t.timestamps
    end

    add_index :academic_infos, :user_id
  end
end
