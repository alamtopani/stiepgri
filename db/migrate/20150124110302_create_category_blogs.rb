class CreateCategoryBlogs < ActiveRecord::Migration
  def change
    create_table :category_blogs do |t|
      t.string :name
      t.string :ancestry

      t.timestamps
    end
  end
end
