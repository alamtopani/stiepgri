class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :website
      t.integer :user_id
      t.text :description

      t.timestamps
    end
    add_index :companies, :user_id
  end
end
