class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.text :address
      t.string :city
      t.string :province
      t.string :postcode
      t.string :addressable_type
      t.integer :addressable_id

      t.timestamps
    end
    add_index :addresses, :addressable_type
    add_index :addresses, :addressable_id
  end
end
