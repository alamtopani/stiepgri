class CreateGuestBooks < ActiveRecord::Migration
  def change
    create_table :guest_books do |t|
      t.string :name
      t.string :email
      t.text :message
      t.boolean :status

      t.timestamps
    end
  end
end
