class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :full_name
      t.date :birthday
      t.string :gender
      t.string :phone
      t.attachment :avatar

      t.timestamps
    end

    add_index :profiles, :user_id
  end
end
