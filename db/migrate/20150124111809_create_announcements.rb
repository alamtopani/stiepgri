class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.string :title
      t.text :description
      t.boolean :status
      t.integer :user_id

      t.timestamps
    end

    add_index :announcements, :user_id
  end
end
