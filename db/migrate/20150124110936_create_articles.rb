class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :description
      t.attachment :file
      t.integer :user_id
      t.boolean :status
      t.integer :category_id
      t.string :category_type

      t.timestamps
    end

    add_index :articles, :category_id
  end
end
