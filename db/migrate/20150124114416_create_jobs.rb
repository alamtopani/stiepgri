class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :position
      t.text :description
      t.date :start_at
      t.date :end_at
      t.boolean :status
      t.integer :user_id
      t.attachment :image
      t.string :website

      t.timestamps
    end
    add_index :jobs, :user_id
  end
end
