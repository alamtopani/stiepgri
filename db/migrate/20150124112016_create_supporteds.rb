class CreateSupporteds < ActiveRecord::Migration
  def change
    create_table :supporteds do |t|
      t.string :title
      t.attachment :file
      t.string :link_url

      t.timestamps
    end
  end
end
