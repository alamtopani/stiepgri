module SeedWebSetting
	def self.seed 
		setting = WebSetting.find_or_initialize_by(email: 'stiepgri@gmail.com')
		setting.title = "STIEPGRI KOTA SUKABUMI"
    setting.description =  ""
    setting.keywords =  "stiepgri, sukabumi, ekonomi sukabumi, akuntansi sukabumi, manajemen ekonomi"
    setting.header_tags =  'We are stiepgri'
    setting.footer_tags =  '&copy; Copyright 2014. STIE PGRI Kota Sukabumi by <a href="http://creativedesignjakarta.com">Creative Design Jakarta</a>'
    setting.contact =  '0266239880'
    setting.facebook =  'http://facebook.com'
    setting.twitter =  'http://facebook.com'
		setting.save
	end
end